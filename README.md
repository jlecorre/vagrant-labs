# Vagrant

* Website : [vagrantup.com](https://www.vagrantup.com/)
* Vagrant Boxes Catalog : [atlas.hashicorp.com](https://atlas.hashicorp.com/boxes/search)

## Description *(from Vagrant website)*

Vagrant is a tool for building and distributing development environments.

Development environments managed by Vagrant can run on local virtualized platforms such as VirtualBox or
VMware, in the cloud via AWS or OpenStack, or in containers such as with Docker or raw LXC.

Vagrant provides the framework and configuration format to create and manage complete portable development
environments. These development environments can live on your computer or in the cloud, and are portable
between Windows, Mac OS X, and Linux.

## Catalog of boxes in this repository

* centos/7 with custom settings
* debian/jessie64 with custom settings
* debian/stretch64 with custom settings
* debian/buster64 with custom settings
* docker image for CentOS 7.3.1611 release __(work in progress)__  

## Extra

* Ignore the `.vagrant` directory :

I use a `core.excludesFile` in my Git configuration, so if you want to ignore the `.vagrant` directory in your
code repository, it's useful to add this lines in your `.gitignore` file :

```shell
# Vagrant context files
#######################
**/.vagrant
```
